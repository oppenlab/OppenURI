package oppen.oppenuri

import org.junit.Assert
import org.junit.Test

class OppenURITests {

    @Test
    fun `set initial address with constructor argument`() {
        val ouri = OppenURI("gemini://oppen.gemini")
        Assert.assertEquals("gemini://oppen.gemini", ouri.toString())
        Assert.assertEquals("oppen.gemini", ouri.host)
    }

    @Test
    fun `set initial address with empty constructor`() {
        val ouri = OppenURI()
        ouri.set("gemini://oppen.gemini")
        Assert.assertEquals("gemini://oppen.gemini", ouri.toString())
        Assert.assertEquals("oppen.gemini", ouri.host)
    }

    @Test
    fun `navigate to new host`() {
        val ouri = OppenURI("gemini://oppen.gemini")
        ouri.resolve("gemini://gus.guru/")
        Assert.assertEquals("gemini://gus.guru/", ouri.toString())
        Assert.assertEquals("gus.guru", ouri.host)
    }

    @Test
    fun `navigate to new host no trailing slash`() {
        val ouri = OppenURI("gemini://oppen.gemini")
        ouri.resolve("gemini://gus.guru")
        Assert.assertEquals("gemini://gus.guru", ouri.toString())
        Assert.assertEquals("gus.guru", ouri.host)
    }

    @Test
    fun `basic navigation`() {
        val ouri = OppenURI("gemini://oppen.gemini")
        ouri.resolve("software/")
        Assert.assertEquals("gemini://oppen.gemini/software/", ouri.toString())
    }

    @Test
    fun `path navigation`() {
        val ouri = OppenURI("gemini://oppen.gemini/software")
        ouri.resolve("/generative/")
        Assert.assertEquals("gemini://oppen.gemini/generative/", ouri.toString())
    }

    @Test
    fun `path navigation to relative`() {
        val ouri = OppenURI("gemini://oppen.gemini/software")
        ouri.resolve("generative/")
        Assert.assertEquals("gemini://oppen.gemini/generative/", ouri.toString())
    }

    @Test
    fun `traverse up and down simple`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("../phaedra/")
        Assert.assertEquals("gemini://oppen.gemini/software/phaedra/", ouri.toString())
    }

    @Test
    fun `traverse up and down complex`() {
        val ouri = OppenURI("gemini://oppen.gemini/alpha/beta/gamma/delta/epsilon/")

        ouri.resolve("../../omega/")
        Assert.assertEquals("gemini://oppen.gemini/alpha/beta/gamma/omega/", ouri.toString())

        ouri.resolve("../../psi/")
        Assert.assertEquals("gemini://oppen.gemini/alpha/beta/psi/", ouri.toString())
    }

    @Test
    fun `traverse up and down complex from a leaf`() {
        val ouri = OppenURI("gemini://oppen.gemini/alpha/beta/gamma/delta/epsilon")

        ouri.resolve("../../omega/")
        Assert.assertEquals("gemini://oppen.gemini/alpha/beta/omega/", ouri.toString())

        ouri.resolve("../../psi/")
        Assert.assertEquals("gemini://oppen.gemini/alpha/beta/psi/", ouri.toString())
    }


    @Test
    fun `traverse up`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("../")
        Assert.assertEquals("gemini://oppen.gemini/software/", ouri.toString())
    }

    @Test
    fun `traverse up script kiddy`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("../../../../../")
        Assert.assertEquals("gemini://oppen.gemini/", ouri.toString())
    }

    @Test
    fun `back to root`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("/")
        Assert.assertEquals("gemini://oppen.gemini/", ouri.toString())
    }

    @Test
    fun `gemtext file reference`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("/index.gmi")
        Assert.assertEquals("gemini://oppen.gemini/index.gmi", ouri.toString())

        ouri.set("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("../index.gmi")

        Assert.assertEquals("gemini://oppen.gemini/software/index.gmi", ouri.toString())
    }

    @Test
    fun `ariane issue 40`() {
        //https://codeberg.org/oppenlab/Ariane/issues/40
        val ouri = OppenURI("gemini://midnight.pub/posts/151")
        ouri.resolve("/~m150")
        Assert.assertEquals("gemini://midnight.pub/~m150", ouri.toString())
    }

    @Test
    fun `traverse up method`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        Assert.assertEquals("gemini://oppen.gemini/software/", ouri.traverse().toString())
    }

    @Test
    fun `successive files`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("a.jpeg")
        Assert.assertEquals("gemini://oppen.gemini/software/ariane/a.jpeg", ouri.toString())

        ouri.resolve("b.jpeg")
        Assert.assertEquals("gemini://oppen.gemini/software/ariane/b.jpeg", ouri.toString())
    }

    @Test
    fun `file then traverse up`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("a.jpeg")
        Assert.assertEquals("gemini://oppen.gemini/software/ariane/a.jpeg", ouri.toString())

        ouri.resolve("../")
        Assert.assertEquals("gemini://oppen.gemini/software/", ouri.toString())
    }

    @Test
    fun `file then root path`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("a.jpeg")
        Assert.assertEquals("gemini://oppen.gemini/software/ariane/a.jpeg", ouri.toString())

        ouri.resolve("/")
        Assert.assertEquals("gemini://oppen.gemini/", ouri.toString())
    }

    @Test
    fun `file then path`() {
        val ouri = OppenURI("gemini://oppen.gemini/software/ariane/")
        ouri.resolve("a.jpeg")
        Assert.assertEquals("gemini://oppen.gemini/software/ariane/a.jpeg", ouri.toString())

        ouri.resolve("ariane_subfolder/")
        Assert.assertEquals("gemini://oppen.gemini/software/ariane/ariane_subfolder/", ouri.toString())
    }

    @Test
    fun `navigate with no host path separator`() {
        val ouri = OppenURI("gemini://oppen.digital")
        ouri.resolve("gemini/")
        Assert.assertEquals("gemini://oppen.digital/gemini/", ouri.toString())

    }
}
