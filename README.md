# OppenURI

A Kotlin class for handling Gemini URIs

The sole purpose of this project is to fix the endless uri/path related issues occuring in [Ariane](https://codeberg.org/oppenlab/Ariane/issues?type=all&state=open&labels=&milestone=0&assignee=0&q=path).

Implementation: [OppenURI.kt](https://codeberg.org/oppenlab/OppenURI/src/branch/main/app/src/main/java/oppen/oppenuri/OppenURI.kt)  
Tests: [OppenURITests.kt](https://codeberg.org/oppenlab/OppenURI/src/branch/main/app/src/test/java/oppen/oppenuri/OppenURITests.kt)